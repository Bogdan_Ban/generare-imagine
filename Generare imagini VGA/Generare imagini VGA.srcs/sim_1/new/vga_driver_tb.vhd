----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/07/2019 03:34:16 PM
-- Design Name: 
-- Module Name: vga_driver_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_driver_tb is
--  Port ( );
end vga_driver_tb;

architecture Behavioral of vga_driver_tb is

signal Clk : STD_LOGIC;
signal RST : STD_LOGIC := '0';
signal HSYNC : STD_LOGIC;
signal VSYNC : STD_LOGIC;
signal R,G,B : STD_LOGIC_VECTOR (3 downto 0);

begin

gen_clk: process
begin
     Clk <= '0';
     wait for 20 ns;
     Clk <= '1';
     wait for 20 ns;
 end process gen_clk;

LUT: entity WORK.vga_driver PORT MAP(
    CLK => Clk,
    RST => RST,
    HSYNC => HSYNC, 
    VSYNC => VSYNC, 
    R => R,
    G => G,
    B => B
 );

end Behavioral;

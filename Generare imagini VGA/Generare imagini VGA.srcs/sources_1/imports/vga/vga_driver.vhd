----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:17:27 12/28/2016 
-- Design Name: 
-- Module Name:    vga_driver - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_driver is
    Port ( CLK : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           HSYNC : out  STD_LOGIC;
           VSYNC : out  STD_LOGIC;
           R,G,B : out  STD_LOGIC_VECTOR (3 downto 0));
end vga_driver;

architecture Behavioral of vga_driver is

	signal clk25 : std_logic := '0';
	
	constant HD : integer := 639;  --  639   Horizontal Display (640)
	constant HFP : integer := 16;         --   16   Right border (front porch)
	constant HSP : integer := 96;       --   96   Sync pulse (Retrace)
	constant HBP : integer := 48;        --   48   Left boarder (back porch)
	
	constant VD : integer := 479;   --  479   Vertical Display (480)
	constant VFP : integer := 10;       	 --   10   Right border (front porch)
	constant VSP : integer := 2;				 --    2   Sync pulse (Retrace)
	constant VBP : integer := 33;       --   33   Left boarder (back porch)
	
	signal hPos : integer := 0;
	signal vPos : integer := 0;
	
	signal videoOn : std_logic := '0';
    
    --Signals for BRAM
    --Signals for Block RAM
    signal wea : STD_LOGIC_VECTOR(0 DOWNTO 0):="0";
    signal addra : STD_LOGIC_VECTOR(15 DOWNTO 0):=(others=>'0');
    signal dina : STD_LOGIC_VECTOR(11 DOWNTO 0):=(others=>'0');
    signal douta : STD_LOGIC_VECTOR(11 DOWNTO 0):=(others=>'0');
    signal we_aux : STD_LOGIC := '0';
    
    --3 RAMs for R,G,B
    type RAM is array (0 to 255) of std_logic_vector(0 to 255);
    signal matrixR, matrixG, matrixB: RAM := (others => (others => '0'));
    
    
    component imageRAM is
  Port (
    clock   : in  std_logic;
    we      : in  std_logic;
    address : in  std_logic_vector(15 downto 0);
    datain  : in  std_logic_vector(11 downto 0);
    dataout : out std_logic_vector(11 downto 0)
  );
end component;
    
begin

-- 100MHz to 25 MHZ        pixel frequency
clk_div:process(CLK)
variable aux : integer := 0;
begin
	if(CLK'event and CLK = '1')then
	   if  aux = 1 then
		   clk25 <= not clk25;
		   aux := 0;
	   else
	       aux := aux + 1;  
       end if;
	end if;
end process;

U3: imageRAM Port map (clk25, we_aux, addra, dina, douta);

Horizontal_position_counter:process(clk25, RST)
begin
	if(RST = '1')then
		hpos <= 0;
	elsif(clk25'event and clk25 = '1')then
		if (hPos = (HD + HFP + HSP + HBP)) then
			hPos <= 0;
		else
			hPos <= hPos + 1;
		end if;
	end if;
end process;

Vertical_position_counter:process(clk25, RST, hPos)
begin
	if(RST = '1')then
		vPos <= 0;
	elsif(clk25'event and clk25 = '1')then
		if(hPos = (HD + HFP + HSP + HBP))then
			if (vPos = (VD + VFP + VSP + VBP)) then
				vPos <= 0;
			else
				vPos <= vPos + 1;
			end if;
		end if;
	end if;
end process;

Horizontal_Synchronisation:process(clk25, RST, hPos)
begin
	if(RST = '1')then
		HSYNC <= '0';
	elsif(clk25'event and clk25 = '1')then
		if((hPos <= (HD + HFP)) OR (hPos > HD + HFP + HSP))then
			HSYNC <= '1';
		else
			HSYNC <= '0';
		end if;
	end if;
end process;

Vertical_Synchronisation:process(clk25, RST, vPos)
begin
	if(RST = '1')then
		VSYNC <= '0';
	elsif(clk25'event and clk25 = '1')then
		if((vPos <= (VD + VFP)) OR (vPos > VD + VFP + VSP))then
			VSYNC <= '1';
		else
			VSYNC <= '0';
		end if;
	end if;
end process;

video_on:process(clk25, RST, hPos, vPos)
begin
	if(RST = '1')then
		videoOn <= '0';
	elsif(clk25'event and clk25 = '1')then
		if(hPos <= HD and vPos <= VD)then
			videoOn <= '1';
		else
			videoOn <= '0';
		end if;
	end if;
end process;

--RED SQUARE
--draw:process(clk25, RST, hPos, vPos, videoOn)
--begin
--	if(RST = '1')then
--		R <= "0000";
--		G <= "0000";
--		B <= "0000";
--	elsif(clk25'event and clk25 = '1')then
--		if(videoOn = '1')then
--			if((hPos >= 100 and hPos <= 200) AND (vPos >= 100 and vPos <= 200))then
--				R <= "1000";
--				G <= "0000";
--				B <= "0000";
--			else
--				R <= "0000";
--		        G <= "0000";
--		        B <= "0000";
--			end if;
--		else
--			R <= "0000";
--		    G <= "0000";
--		    B <= "0000";
--		end if;
--	end if;
--end process;


--draw:process(clk25, RST, videoOn)
--begin
--	if(RST = '1')then
--		R <= "0000";
--		G <= "0000";
--		B <= "0000";
--	elsif(clk25'event and clk25 = '1')then
--		if(videoOn = '1')then
--            if (unsigned(addra)<65536) then
--                R <= "00" & douta(7 downto 6); 
--                G <= '0' & douta(5 downto 3); 
--                B <= '0' & douta(2 downto 0);
--                addra<=STD_LOGIC_VECTOR(unsigned(addra)+1);
--			else
--				R<=(others=>'0');G<=(others=>'0');B<=(others=>'0');
--			end if;
--		else
--			R<=(others=>'0');G<=(others=>'0');B<=(others=>'0');
--			addra<=(others=>'0');
--		end if;
--	end if;
--end process;

--draw:process(clk25, RST, videoOn)
--begin
--	if(RST = '1')then
--		R <= "0000";
--		G <= "0000";
--		B <= "0000";
--	elsif(clk25'event and clk25 = '1')then
--		if(videoOn = '1')then
--            if (hPos >= 100 and hPos <= 355) then
--                if (vPos >= 100 and vPos <= 355) then 
--                    R <= "0000"; 
--                    G <= douta(7 downto 4); 
--                    B <= douta(3 downto 0);
--                    --addra<=STD_LOGIC_VECTOR(unsigned(addra)+1);
--                    addra <= addra + '1';
--                end if;
--			else
--				R<=(others=>'0');G<=(others=>'0');B<=(others=>'0');
--			end if;
--		else
--			R<=(others=>'0');
--			G<=(others=>'0');
--			B<=(others=>'0');
--            addra <= (others => '0');
--		end if;
--	end if;
--end process;

drawGOOD: process(clk25, RST, videoOn)
begin
	if(RST = '1')then
		R <= "0000";
		G <= "0000";
		B <= "0000";
	elsif(clk25'event and clk25 = '1')then
		if(videoOn = '1')then
            if (hPos <= 256 and vPos <= 256) then
                    R <= douta(2 downto 0) & '0'; 
                    G <= douta(5 downto 3) & '0'; 
                    B <= douta(7 downto 6) & "00";
                    addra<=STD_LOGIC_VECTOR(unsigned(addra)+1);
                    --addra <= addra + '1';
            else
                    R<=(others=>'0');G<=(others=>'0');B<=(others=>'0');
            end if;
        else
				R<=(others=>'0');G<=(others=>'0');B<=(others=>'0');
        end if;
		if (hPos = (HD + HFP + HSP + HBP) and hPos = (VD + VFP + VSP + VBP)) then
            addra <= (others => '0');
		end if;
	end if;
end process;

end Behavioral;

